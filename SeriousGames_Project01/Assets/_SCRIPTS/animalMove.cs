﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animalMove : MonoBehaviour
{

    public Vector3 animOrigin;
    public bool animSnap;
	public bool wasSnaped;
	private counters counterUndo;

    // Use this for initialization
    void Start()
    {
        animOrigin = this.transform.position;
        animSnap = false;
		wasSnaped = false;
		counterUndo = GameObject.Find("undoCounter").GetComponent<counters>();
    }

    // Update is called once per frame
    void Update()
    {
		if(animSnap){
			wasSnaped = true;
		}
		if(!animSnap && wasSnaped){
			wasSnaped = false;
			counterUndo.takeTurn();

		}
    }

    void OnMouseDrag()
    {
        animSnap = false;
        Vector3 moveTo = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        transform.position = new Vector3(moveTo.x, moveTo.y, transform.position.z);
    }
    
    void OnMouseUp()
    {
        if (!animSnap)
        {
            this.transform.position = animOrigin;
        }
    }
    
}