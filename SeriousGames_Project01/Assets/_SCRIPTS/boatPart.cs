﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class boatPart : MonoBehaviour {

	public List<GameObject> animal = new List<GameObject>();
    private counters counter;

    // Use this for initialization
    void Start () {
        counter = GameObject.Find("turnCounter").GetComponent<counters>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other){
        if (Input.GetMouseButtonUp(0) == true) {
            if (other.tag == "animalPiece")
            {
                //Result = The current value of the boat - the animal size
                int result = Convert.ToInt32((this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().lineSpacing) - Convert.ToInt32(other.gameObject.GetComponentInChildren<UnityEngine.UI.Text>().text);

                //Check if boat value would go below zero, don't snap if it does
                if (result >= 0)
                {
                    counter.takeTurn();
                    other.GetComponent<animalMove>().animSnap = true;
                    if (result == 0)
                    {
                        (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().color = new Color(0f, 1f, 0f);
                    }
                    else
                    {
                        (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().color = new Color(1f, 1f, 0f);
                    }
                    (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().lineSpacing = result;
                    animal.Add(other.gameObject);
                    other.transform.position = this.transform.position;
                    other.tag = "subAnimal";
                }
                else
                {
                    other.GetComponent<animalMove>().animSnap = false;
                }
                //Size of animal
                //Debug.Log(other.gameObject.GetComponentInChildren<UnityEngine.UI.Text>().text);

                //Size of boat
                //Debug.Log((this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().text);
                
            }
        }
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "subAnimal") {

            other.tag = "animalPiece";
            int result = Convert.ToInt32((this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().lineSpacing) + Convert.ToInt32(other.gameObject.GetComponentInChildren<UnityEngine.UI.Text>().text);
            if (result.ToString() == (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().text)
            {
                (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().color = new Color(1f, 0f, 0f);
            }
            else
            {
                (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().color = new Color(1f, 1f, 0f);
            }
            (this.transform.parent).GetComponentInChildren<UnityEngine.UI.Text>().lineSpacing = result;


            animal.Remove(other.gameObject);
        }
	}



}
