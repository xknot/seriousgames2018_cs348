﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class _init : MonoBehaviour {

	public int rootNumberOfPoints;
    public GameObject turnCount;
	public GameObject undoCount;
	public int turns;
	public int undos;

	public GameObject hull;
	public GameObject mid;
	public GameObject back;
	public GameObject animal1;
	public GameObject animal2;
	public GameObject animal3;
	public GameObject animal4;
	public GameObject animal5;
	public GameObject boat;
	private GameObject tempBoat;

	public float xSpace = 1.5f;
	public float ySpace = 1.6f;

	private float fieldMaxX = 0f;
	private float fieldMaxY = 3.75f;

	public float xSpace2 = 1.2f;
	public float ySpace2 = 2f;

	public float fieldMaxX2 = -7.93f;
	public float fieldMaxY2 = 3.91f;

	bool won;

	// Use this for initialization
	void Start () {
		buildField ();

	}
	
	// Update is called once per frame
	void Update () {

		GameObject[] boats = GameObject.FindGameObjectsWithTag ("boat");
		won = true;
		for (int i = 0; i < boats.Length; i++) {
			if (Convert.ToInt32(boats [i].GetComponentInChildren<UnityEngine.UI.Text> ().text) != 0) {
				won = false;
			}
		}

		if (won) {
			playerWon ();
		}

		
	}

	public void buildField() {
		won = false;

		//Destroys field population
		GameObject[] fieldPop;
		fieldPop = GameObject.FindGameObjectsWithTag ("boatPiece");
		for (int i = 0; i < fieldPop.Length; i++) {
			Destroy (fieldPop [i]);
		}
		fieldPop = GameObject.FindGameObjectsWithTag ("boat");
		for (int i = 0; i < fieldPop.Length; i++) {
			Destroy (fieldPop [i]);
		}


		//Creates the field
		GameObject previous = back;
		GameObject piece = back;
		List<int> boatList = new List<int>();
		int boatLength = 0;

		for (int j = 0; j <= rootNumberOfPoints; j++) {
			for (int i = 0; i <= rootNumberOfPoints; i++) {
				if (i == 0) {
					piece = back;
					boatLength = 1;
				} else if (i == rootNumberOfPoints) {
					if (previous != hull) {
						piece = hull;
						boatLength++;
						boatList.Add (boatLength);
                        tempBoat.transform.Find("Canvas").Find("BoatSize").GetComponent<UnityEngine.UI.Text>().text = boatLength.ToString();
                        tempBoat.transform.Find("Canvas").Find("BoatSize").GetComponent<UnityEngine.UI.Text>().lineSpacing = boatLength;
                    } else {
						piece = null;
					}
				} else {
					if (previous == hull) {
						piece = back;
						boatLength = 1;
					} else {
						int r = UnityEngine.Random.Range (1, 3);
						if (r == 1) {
							piece = mid;
							boatLength++;
						} else {
							piece = hull;
							boatLength++;
							boatList.Add (boatLength);
                            tempBoat.transform.Find("Canvas").Find("BoatSize").GetComponent<UnityEngine.UI.Text>().text = boatLength.ToString();
                            tempBoat.transform.Find("Canvas").Find("BoatSize").GetComponent<UnityEngine.UI.Text>().lineSpacing = boatLength;
                        }
					}
				}
				if (piece != null) {
					GameObject temp = Instantiate (piece, new Vector3 (fieldMaxX + (xSpace * j), fieldMaxY - (ySpace * i), 0f), Quaternion.identity);
					if (piece == back) {
						tempBoat = Instantiate (boat, new Vector3 (fieldMaxX + (xSpace * j), fieldMaxY - (ySpace * i), 0f), Quaternion.identity);
						temp.transform.SetParent (tempBoat.transform);
                        
					} else {
						temp.transform.SetParent (tempBoat.transform);
					}
				}
				previous = piece;
		    }
		    createAnimals (boatList);
		}
	}



	public void createAnimals(List<int> boatList) {

		List<int> animalList = new List<int>();

		//Destroys animal population
		GameObject[] fieldPop;
		fieldPop = GameObject.FindGameObjectsWithTag ("animalPiece");
		for (int i = 0; i < fieldPop.Length; i++) {
			Destroy (fieldPop [i]);
		}
        fieldPop = GameObject.FindGameObjectsWithTag("subAnimal");
        for (int i = 0; i < fieldPop.Length; i++)
        {
            Destroy(fieldPop[i]);
        }


        for (int j = 0; j < boatList.Count; j++) {

			int temp = boatList[j];

			while (temp > 0) {

				if (temp == 1) {
					animalList.Add (temp);
					temp = 0;
				} else {
					int i = UnityEngine.Random.Range (0, 2);
					if(i >= 1){
						animalList.Add (temp);
						temp = 0;
					}else{
						int r = UnityEngine.Random.Range (1, temp + 1);
						animalList.Add (r);
						temp = temp - r;
					}
				}
			}


		}

		foreach(int i in animalList){
			if (i == 1) {
                Instantiate (animal1, new Vector3 (fieldMaxX2 + xSpace2 * i, fieldMaxY2 - ySpace2, 0f), Quaternion.identity);
			}
			if (i == 2) {
				Instantiate (animal2, new Vector3 (fieldMaxX2 + xSpace2 * i, fieldMaxY2 - ySpace2, 0f), Quaternion.identity);
			}
			if (i == 3) {
				Instantiate (animal3, new Vector3 (fieldMaxX2 + xSpace2 * i, fieldMaxY2 - ySpace2, 0f), Quaternion.identity);
			}
			if (i == 4) {
				Instantiate (animal4, new Vector3 (fieldMaxX2 + xSpace2 * (i-3), fieldMaxY2 - ySpace2 * 2, 0f), Quaternion.identity);
			}
			if (i == 5) {
				Instantiate (animal5, new Vector3 (fieldMaxX2 + xSpace2 * (i-3), fieldMaxY2 - ySpace2 * 2, 0f), Quaternion.identity);
			}
		}
        setMoves(animalList.Count);

	}

    public void setMoves(int num)
    {
        turnCount.GetComponent<UnityEngine.UI.Text>().text = num.ToString();
		undoCount.GetComponent<UnityEngine.UI.Text>().text = "3";
    }


	public void playerWon(){
		Debug.Log ("player won");
	}
}
