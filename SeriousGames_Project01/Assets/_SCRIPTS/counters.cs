﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class counters : MonoBehaviour {

	public GameObject gm;

	private int undosMax;
	private int turnsMax;


	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    //Working turn counter
    public void takeTurn()
    {
		//Debug.Log(this.name);
        if (this.name == "turnCounter")
        {
            Debug.Log("Take Turn");
            this.GetComponent<UnityEngine.UI.Text>().text = (Convert.ToInt32(this.GetComponent<UnityEngine.UI.Text>().text) - 1).ToString();

            if (this.GetComponent<UnityEngine.UI.Text>().text == "0")
            {
                Debug.Log("End Game");
                SceneManager.LoadScene("Scn_Win");
                //End Game State
            }
        }

		if (this.name == "undoCounter")
		{
			Debug.Log("Take Undo");
			this.GetComponent<UnityEngine.UI.Text>().text = (Convert.ToInt32(this.GetComponent<UnityEngine.UI.Text>().text) - 1).ToString();

			if (this.GetComponent<UnityEngine.UI.Text>().text == "0")
			{
				Debug.Log("End Game");
				//End Game State
			}
		}
    }
}
