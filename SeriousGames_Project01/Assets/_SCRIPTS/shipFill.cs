﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class shipFill : MonoBehaviour {

	private List<GameObject> animals = new List<GameObject>();
	private List<GameObject> temp = new List<GameObject>();
	public int size;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

		foreach (Transform child in transform)
		{
            if (child.tag == "boatPiece")
            {
                temp.AddRange(child.gameObject.GetComponent<boatPart>().animal);
            }
	
		}
		animals.AddRange(temp.Distinct().ToList());

		foreach (GameObject animal in animals) {
			//Debug.Log (animal.name);
		}
		animals.Clear();
		temp.Clear();

		
	}


}
