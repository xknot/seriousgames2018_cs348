﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class OnClickLoad : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene("Experimental_01");
    }
}