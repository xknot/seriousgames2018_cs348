﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickAudio : MonoBehaviour {

    AudioSource audioSource;
    Animation anim;

    void Start() {
    	anim = gameObject.GetComponent<Animation>();
    	audioSource = gameObject.GetComponent<AudioSource>();
    }

	// Update is called once per frame
	void OnMouseDown () {
        audioSource.Play();
        anim.Play();
	}
}
